%Draw rectangles
function im = rectDraw(im,bbs,colorOpt)

    szBB = size(bbs);

    for i=1:szBB(1)
        shapeInserter = vision.ShapeInserter('LineWidth',7,'Shape','Rectangles','BorderColor','Custom','CustomBorderColor',colorOpt(bbs(i,6),:));
        im = step(shapeInserter,im,int32(bbs(i,1:4)));
        im = insertText(im,[bbs(i,1)+bbs(i,3),bbs(i,2)],num2str(bbs(i,5),4),'FontSize',42);
    end

end