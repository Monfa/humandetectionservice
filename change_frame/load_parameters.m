function P = load_parameters(k)

	n_cam = 4;
	for n = 0:(n_cam-1)
	    load(['ptgrey.' num2str(n) '/intrinsic.mat'],'-ASCII');
	    load(['ptgrey.' num2str(n) '/extrinsic.mat'],'-ASCII');
	    intrinsic(1:2,:) = intrinsic(1:2,:) / k;
	    P{n+1}.intrinsic = intrinsic;
	    P{n+1}.extrinsic = extrinsic;
	end

end
