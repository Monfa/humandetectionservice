function zC = z_coord(ptsPix, cam, P, z)
    A_cam = P{cam+1}.intrinsic*eye(3,4)*P{cam+1}.extrinsic;
    ptsWorld = inv([A_cam(:,1:2) -ptsPix])*(-z*A_cam(:,3) - A_cam(:,4));
    zC=ptsWorld(3);