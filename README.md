![](logo.jpeg)

# README #

Code Related to the manuscript **A flexible human detection service suitable for Intelligent Spaces based on
a multi-camera network**: [article](http://journals.sagepub.com/doi/abs/10.1177/1550147718763550);

# **IMAGES**

The images related to the article are available at: [dataset](https://drive.google.com/open?id=1NWax3pSWhjTRJAfAZ7EyGRCTBFAqA63r);

The calibration data of the cameras (intrinsic and extrinsic parameters) are available together with the code, in the folder /change_frame/ptgrey.X, where X stands for the number of the camera X={0,1,2,3};

Please, if you use these images or the code, please, consider citing our article as indicated bellow.

# **INSTRUCTIONS**

This code has the following dependencies:

>**Piotr's Computer Vision Matlab Toolbox:**

 >>Version: 3.50 tested;
 
 >>[download](https://github.com/pdollar/toolbox);
 
 >>Used resources: Functions related to pedestrian detection are used;
 
 >>>These functions are needed to run the detectors of [1] (ICCF) and [2] (ACF).
 
 >>Please, if you use the Toolbox consider citing the original source.

To execute:

>**Run the code**:
 >>Run humanDetectionMatlabSample.m. This script will use the video in the input/ folder. An output video will be generated in the output/ folder.

>**Observation**
 >>This is only a Matlab sample code of the Human Localization and BB Filtering services. This sample code is provided for quick execution and analysis of the services' implementation. These services are implemented as Matlab functions here. However, inside our IS architecture, they are implemented inside Docker containers as explained in the paper. They are completely independent (including its instances) inside our IS architecture as described in the manuscript.
 
 >>In the external folder, there is a modified version of the Piotr's Computer Vision Matlab Toolbox.

# **CITATION INSTRUCTIONS**
If you use the code or images, please consider citing:

    @article{doi:10.1177/1550147718763550,
    author = {Douglas Almonfrey and Alexandre Pereira do Carmo and Felippe Mendon�a de Queiroz and Rodolfo Picoreti and Raquel Frizera Vassallo and Evandro Ottoni Teatini Salles},
    title ={A flexible human detection service suitable for Intelligent Spaces based on a multi-camera network},
    journal = {International Journal of Distributed Sensor Networks},
    volume = {14},
    number = {3},
    pages = {1550147718763550},
    year = {2018},
    doi = {10.1177/1550147718763550},
    URL = { 
            https://doi.org/10.1177/1550147718763550
    },
    eprint = { 
            https://doi.org/10.1177/1550147718763550 
    },
    }

[1] - [Almonfrey D, Vassallo RF, Salles EOT et al. Neural cells insights on pedestrian detection. In CBA 2016 - XXI Brazilian Conference of Automation.](http://www.swge.inf.br/proceedings/paper/?P=CBA2016-0590)

[2] - Dollár P, Appel R, Belongie S et al. Fast feature pyramids for object detection. IEEE Transactions on Pattern Analysis and Machine Intelligence 2014; 36: 1532-1545.

# **ACKNOWLEDGEMENT**

We would like to thanks Thales Layber by the help in the annotation of the images.

Finally, follows the website of our lab: [VIROS](http://viros.ufes.br/)

