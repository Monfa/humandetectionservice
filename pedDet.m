function [bbs,pts,im1] = pedDet(quadro, imBackGnd, detector, detectorProp, varargin)
    
    %INPUT
    %backGndSub - [1] - if 1, uses background subtraction
    %numProp - [1] - number of proposals used by the main detector (In the
    %paper ICCF) that are returned by the proposal detector (In the paper
    %ACF)
    %justProp - [0] - if 1, just detection proposal is used (In the paper
    %ACF)
    %ftR - [3] -  Image downsampling factor
    %smooth - [0] - if 1, smooth the image
    %thrsSub - [0] - Minimum value of intensity considered by the
    %background subtraction image. Values above this threshold are
    %considered equal zero
    %valTrsf - [0] - One BB is projected to other camera image only if it 
    %has at least one corresponding BB in the referred camera
    %minMatch - [1] - How many matches a BB must have to not be considered
    %a false positive
    %elimFP - [0] - If 1, discard BB that doesn't have at least a minMatch
    %camNum - [0] - The vector specifying the camera numbers. For four
    %cameras must be set to [0, 1, 2, 3].
    
    %OUTPUT
    %bbs - BBs in the reference frame of the input image "quadro"
    %im1 - background image downsampled by ftR
    
    %define defaults and read input
    dfs={'backGndSub',1, 'numProp',1, 'justProp',0, 'ftR',3, 'smooth', 0, 'thrsSub', 0, 'thrComp', 0.5,'valTrsf',0,'minMatch',1,'elimFP',0,'camNum',0};
    [backGndSub,numProp,justProp,ftR,smooth,thrsSub,thrComp,valTrsf,minMatch,elimFP,camNum]=getPrmDflt(varargin,dfs,1);

    detector.opts.pCNN.justProp = justProp;

    P = load_parameters(ftR);

    numVid = length(quadro);
    
    if(backGndSub)
        for w=1:numVid
           imBackGnd{w} = suavOuAmosImagem(imBackGnd{w},smooth,ftR);
        end
    else
        imBackGnd = {};
    end

    %video dimensions
    szIm = size(quadro{1});

    tmp=0;
    for w=1:numVid
        
        quadro{w}=suavOuAmosImagem(quadro{w},smooth,ftR);
        
        if(backGndSub)
            im1{w}=subtraiGnd(quadro{w},imBackGnd{w},thrsSub);
        else
            im1{w}=[];
        end
        
        bbs{w}=detWProp(quadro{w}, im1{w}, detector, detectorProp, numProp, w);
    end

    [bbs,pts] = transformaPontosBBCAM(bbs,szIm,P,numVid,thrComp,valTrsf,camNum);
    
    if(elimFP)
        [bbs] = filtraFP(bbs,numVid,thrComp,minMatch);
    end
    
    %restore image dimensions
    for w=1:numVid
        bbs{w}(:,1:4)=bbs{w}(:,1:4).*ftR;
        pts{w}(:,1:2)=pts{w}(:,1:2).*ftR;
    end
    
    
end

function im1=subtraiGnd(quadro,medIm,thrsSub)

    %Normalize image [0-255]->[0,1] before subtraction
    im1=(abs((double(quadro)-double(medIm)))/255);
    im1=double(sum(im1,3)/3).*double(sum(im1,3)/3>thrsSub);
end

function medIm = suavOuAmosImagem(medIm,smooth,ftR)

    %Smooth image?
    if(smooth)
        medIm=uint8(convTri(single(amostra(medIm,ftR)),smooth));
        medIm=amostra(medIm,ftR);
    else
        medIm=amostra(medIm,ftR);
    end
    
end

function quadro = amostra(quadro,ftR)

    a=quadro(:,:,1);
    b=quadro(:,:,2);
    c=quadro(:,:,3);
    
    a = imResample(a,1/ftR,'bilinear');
    b = imResample(b,1/ftR,'bilinear');
    c = imResample(c,1/ftR,'bilinear');
    
    quadro=cat(3,a,b,c);
end

function [bbs,pts] = transformaPontosBBCAM(bbs,sz,P,numVid,thrComp,valTrsf,camNum)

    vetJ = 1:numVid;
    for i=1:length(vetJ)
        %Number of detections for each camera's image
        numDetVidAd{i} = size(bbs{i},1);
        %TAG BB for each camera
        bbs{vetJ(i)}(:,6)=camNum(vetJ(i))+1;
    end
    bbsS = bbs;
    
    for cont = 1:numVid
        pts{cont} = ones(0,3);
    end
    
    %For each camera, transform its BB to other camera's image
    for w = 1:numVid
        vetJA = vetJ(vetJ~=w);
        
        numDt = size(bbs{w},1);
        for i=1:numDt

            pontoAntes = [bbs{w}(i,1)+bbs{w}(i,3)/2;bbs{w}(i,2)+bbs{w}(i,4);1];

            pts{w}(i,:) = [pontoAntes(1),pontoAntes(2),w];
            
            for j=vetJA
                
                %midpoint of BB base before transformation
                pontoDepois = change_frame(pontoAntes,(w-1),(j-1),P,0);
                
                z_antes = z_coord(pontoAntes,(w-1),P,0);
                z_depois = z_coord(pontoDepois,(j-1),P,0);
                
                scaleFact = z_antes/z_depois;
                
                bbsAux = bbs{w}(i,:);
                
                nw=bbsAux(3).*scaleFact;
                nh=bbsAux(4).*scaleFact;
                
                %midpoint of BB base after transformation
                pontoDepois = [pontoDepois(1)-nw/2;pontoDepois(2)-nh;1];

                if((pontoDepois(1)+nw)<=sz(2) && pontoDepois(1)>0 && (pontoDepois(2)+nh)<=sz(1) && pontoDepois(2)>0)
                    bbsAux = bbs{w}(i,:);
                    bbsAux(1) = pontoDepois(1);
                    bbsAux(2) = pontoDepois(2);
                    bbsAux(3) = nw;
                    bbsAux(4) = nh;
                    bbsAux(6) = w;
                    
                    %If a BB has a match in other camera's image it is
                    %projected to the referred camera's image
                    if(valTrsf)
                        oa = max(bbGt('compOas',bbsAux,bbs{j},zeros(1,size(bbs{j},1))));

                        if(oa>thrComp)
                            numDetVidAd{j}=numDetVidAd{j}+1;
                            bbsS{j}(numDetVidAd{j},:) = bbsAux;

                            pts{j}(numDetVidAd{j},:) = [pontoDepois(1)+nw/2,pontoDepois(2)+nh,w];
                        end
                    else
                        numDetVidAd{j}=numDetVidAd{j}+1;
                        bbsS{j}(numDetVidAd{j},:) = bbsAux;

                        pts{j}(numDetVidAd{j},:) = [pontoDepois(1)+nw/2,pontoDepois(2)+nh,w]; 
                    end
                end
            end
        end
    end
    bbs = bbsS;
end

function [bbs] = filtraFP(bbs,numVid,thrComp,minMatch)
%BB Filtering service implemented as a Matlab sample 

   for w = 1:numVid
        idx = bbs{w}(:,6)==w;
        dt = bbs{w}(idx,:);
        gt = bbs{w}(~idx,:);

        idxGtSv = [];
        idxDtSv = [];
        
        if(~isempty(gt)&&~isempty(dt))
            for j=1:size(dt,1)
                oa = bbGt('compOas',dt(j,:),gt,zeros(1,size(gt,1)));
                oa=oa>thrComp;
                if(sum(oa)>=minMatch);
                    idxGtSv = [idxGtSv find(oa~=0)];
                    idxDtSv = [idxDtSv j];
                end
            end
        end
        
        bbs{w} = [dt(idxDtSv,:);gt(unique(idxGtSv),:)];
   end

end