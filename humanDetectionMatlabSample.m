function humanDetectionMatlabSample()

close all

%path to calibration data and homography related functions
addpath(genpath('change_frame'));
%path to auxiliary functions
addpath(genpath('external/piotr-toolbox'));

%Percentual used of the video 1==100%
percVid = 1;

%Read video file
numVid = 4;
%In the paper start from 1.
camNum = [0,1,2,3];
loadBack=0;
plotBBothers=0;

%Read videos
for w=1:numVid
    vi = VideoReader(['input/ptgrey.' num2str(w-1) '.avi']);
    vid = read(vi);
    vidFrames{w} = vid(:,:,:,1:end*percVid);
    numFrames(w) = size(vidFrames{w},4);
end

szIm = size(vidFrames{1});

%Write video result
viSaida = VideoWriter(['output/videoSaida']);
viSaida.FrameRate = 5;
open(viSaida);

%Load ACF to generate proposals
detectorProp = load('models/AcfInriaDetector.mat');
detectorProp=detectorProp.detector;

pModify=struct('cascThr',-1,'cascCal',0.001);
detectorProp = acfModify_DA(detectorProp,pModify);

%Load ICCF detector model
detector = load('models/icaInriaSeed82-9Detector.mat');
detector = detector.detector;

pModify=struct('cascThr',-1,'cascCal',0.025);
detector=acfModify_DA(detector,pModify);

%Load background subtraction or compute it right now.
if(loadBack)
    ldFile=load('models/imBackGnd.mat');
    for w=1:numVid
        imBackGnd{w} = ldFile.imBackGnd{w};
    end
else
    for w=1:numVid
        imBackGnd{w} = backSubComp(vidFrames{w},min(numFrames));
    end
end

save(['output/imBackGnd.mat'],'imBackGnd');

%Output image
numCols = 3;
numRows = 4;
imOut = uint8(zeros(numRows*szIm(1),numCols*szIm(2),szIm(3)));

%Write results to a video. This slow the process because of Matlab.
exibir = 1;

if(exibir)
    coluna = 1;
    for w=1:numVid
       imOut((w-1)*szIm(1)+1:w*szIm(1),(coluna-1)*szIm(2)+1:coluna*szIm(2),:) = uint8(imBackGnd{w});
    end    
    imHandle = imshow(imOut);
end

colorOpt = [0,255,0; %green
            255,0,0; %red
            255,255,0; %yellow
            0,0,255]; %blue
        
i=1;
numFramesE = floor(min(numFrames));

while(i<numFramesE-1)
    
    for w=1:numVid
        quadro{w}=vidFrames{w}(:,:,:,i);
    end
    
    [bbs,pts,im1] = pedDet(quadro, imBackGnd, detector, detectorProp,'ftR',2.5,'numProp',50,'justProp',0,'backGndSub',1,'valTrsf',0,'thrComp',0.5,'elimFP',1,'camNum',camNum);
    
    ptsTraj{i} = pts;
   
    %Stop if esc is pressed
    tic
    while toc<0.005 %Wait 5 ms
        letra = get(gcf,'CurrentCharacter');
    end
    if(letra=='s')
        break
    end
    
    for w=1:numVid
       if(exibir)

            coluna = 2;
            if(~isempty(bbs{w}))
                %Plot in an image only its BB.
                if(plotBBothers)
                    bbsPlot = bbs{w}(find(bbs{w}(:,6)==w),:);
                else
                    bbsPlot = bbs{w};
                end
                quadro{w} = uint8(rectDraw(quadro{w},bbsPlot,colorOpt));
            end
            imOut((w-1)*szIm(1)+1:w*szIm(1),(coluna-1)*szIm(2)+1:coluna*szIm(2),:) = uint8(quadro{w});

            coluna = 3;
            if(~isempty(im1{w}))
                imOut((w-1)*szIm(1)+1:w*szIm(1),(coluna-1)*szIm(2)+1:coluna*szIm(2),:) = repmat(uint8(imResample(im1{w},[szIm(1) szIm(2)]).*255),1,1,3);
            end
            
       end
    end
    
    if(exibir)
        imOut = insertText(imOut,[1 10],'Back. Image','FontSize',56);
        imOut = insertText(imOut,[1289 10],'Image','FontSize',56);
        imOut = insertText(imOut,[2577 10],'Subtr. Image','FontSize',56);
        imOut = insertText(imOut,[10 650],'Camera 1','FontSize',56);
        imOut = insertText(imOut,[10 1379],'Camera 2','FontSize',56);
        imOut = insertText(imOut,[10 2107],'Camera 3','FontSize',56);
        imOut = insertText(imOut,[10 2835],'Camera 4','FontSize',56);
        
        set(imHandle,'CData',imOut);
        pause(0.0001);
        writeVideo(viSaida,imOut);
    end
        
    i=i+1;
end

close(viSaida);

end
