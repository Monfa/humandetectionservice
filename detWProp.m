%Detect using a detector as proposal (In the paper ACF).
function bbsFinal = detWProp(I, Isub, detector, detectorProp, numProp, w)

    bbs=acfDetect(I,detectorProp);

    numDt = size(bbs,1);
    
    if(~isempty(Isub))
        for i=1:numDt
            Is = bbApply('crop',I,bbs(i,:));
            Is2 = bbApply('crop',Isub,bbs(i,:));
            szIs=size(Is{1});
            pesoSub = sum(sum(sum(bsxfun(@times,double(Is{1}),Is2{1}))))/(255*szIs(1)*szIs(2)*szIs(3));
            bbs(i,5)=bbs(i,5)*pesoSub;
        end
    end

    [~,index]=sort(bbs(:,5),'descend');
    bbs = bbs(index,:);

    bbs=bbs(bbs(:,5)>0.008,:);
    
    %Select just numProp detections returned by the detection proposal
    bbs=bbs(1:min(numProp,size(bbs,1)),:);
    bbs=bbs((bbs(:,5)~=-Inf),:);
    numDt = size(bbs,1);
    
    %Load information about NMS.
    pNms=detector.opts.pNms;
    minDs=detector.opts.pPyramid.minDs;

    %Use another detector to refine candidate BB (In the paper ICCF) 
    if(detector.opts.pCNN.justProp==0)
        bbsFinal = ones(0,5);
        numDtFinal=0;
        for i=1:numDt
            
            Is = bbApply('crop',I,bbs(i,:));
            
            if(size(Is{1},1)>=minDs(1) && size(Is{1},2)>=minDs(2))

                bbsAux=acfDetect(Is{1},detector);
                 
                if(~isempty(bbsAux))
                    
                        bbsAux(:,1) = bbsAux(:,1)+bbs(i,1);
                        bbsAux(:,2) = bbsAux(:,2)+bbs(i,2);
                    
                    numDtFinal=numDtFinal+1;
                    bbsFinal(numDtFinal,1) = bbsAux(1,1);
                    bbsFinal(numDtFinal,2) = bbsAux(1,2);
                    bbsFinal(numDtFinal,3) = bbsAux(1,3);
                    bbsFinal(numDtFinal,4) = bbsAux(1,4);
                    bbsFinal(numDtFinal,5) = bbsAux(1,5)*bbs(i,5);
                    
                end
            end    
        end
        
        [~,index]=sort(bbsFinal(:,5),'descend');
        bbsFinal = bbsFinal(index,:);
        
        bbsFinal=bbsFinal(bbsFinal(:,5)>15,:);
        bbsFinal=bbNms(bbsFinal,pNms);
    else
        bbs=bbNms(bbs,pNms);
        bbsFinal = bbs;
    end
      
end
