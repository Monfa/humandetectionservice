function medIm = backSubComp(vidFrames,numFrames)

    %Make a background model using part of the video
    alpha = 0.05;
    medIm = vidFrames(:,:,:,1);
    %Percentual of images used from the video
    percModBack = (30/100);
    for i = 2:numFrames*percModBack
        medIm = (1-alpha).*medIm+alpha.*vidFrames(:,:,:,i);
    end
end